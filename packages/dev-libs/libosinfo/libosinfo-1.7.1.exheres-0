# Copyright 2012 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require pagure [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require meson
require test-dbus-daemon

SUMMARY="GObject based library API for managing information about operating systems"
HOMEPAGE+=" https://${PN}.org"

LICENCES="GPL-2 LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: af am anp ar as ast bal be bg bn_IN bn bo br brx bs ca cs cy da de_CH de el en_GB eo
               es et eu fa fi fr gl gu he hi hr hu ia id ilo is it ja ka kk km mn ko kw_GB kw
               kw@kkcor kw@uccor ky lt lv mai mk ml mn mr ms nb nds ne nl nn nso or pa pl pt_BR pt
               ro ru si sk sl sq sr sr@latin sv ta te tg th tr tw uk ur vi wba yo zh_CN zh_HK zh_TW
               zu )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.7] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/libxslt[>=1.0.0]
        gnome-desktop/libsoup:2.4
    run:
        sys-apps/osinfo-db-tools
    test+run:
        sys-apps/osinfo-db[>=20190905]
        sys-apps/pciutils-data
        sys-apps/usbutils-data
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-pci-ids-path=/usr/share/misc/pci.ids
    -Dwith-usb-ids-path=/usr/share/misc/usb.ids
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    "gobject-introspection enable-introspection"
    "vapi enable-vala"
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Denable-tests=true -Denable-tests=false'
)

